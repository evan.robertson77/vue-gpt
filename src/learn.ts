import { createApp } from 'vue'
import Learn from './components/learn-home.vue'

createApp(Learn).mount("#learn-div")