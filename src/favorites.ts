import { createApp } from 'vue'
import Faves from './components/favorite-modules.vue'

createApp(Faves).mount("#faves-div")